﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    public WeaponController wepController;
    bool playerInPickupRange;
    GameObject weaponToPickup;

    public PowerupController powerupController;
    bool playerInPowerUpRange;
    GameObject powerupToPickup;

    Transform bulletSpawn;

    float fireRate;
    [HideInInspector] public float reloadTime;
    public int bulletsInMag;
    public int maxBullets;
    NetworkManager netMg;
    Weapon activeWeapon;

    //Inheriting from DoubleDamage script. It's power up that double every bullet damage x2
    [HideInInspector] public bool doubledDamage;

    void Awake()
    {
        if(wepController.equippedWeapons[wepController.activeSlot] != null)
            activeWeapon = wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>();
        netMg = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<NetworkManager>();
        powerupController = gameObject.GetComponent<PowerupController>();
    }

	void Update ()
    {
        #region Getting active weapon

        if (wepController.equippedWeapons[wepController.activeSlot] != null)
            activeWeapon = wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>();
        else
            activeWeapon = null;

        #endregion

        if (!isLocalPlayer)
            return;

        #region Weapon & powerup

        if (Input.GetButtonDown("Pickup") && playerInPickupRange)
        {
            playerInPickupRange = false;
            
            if(weaponToPickup != null)
            {
                wepController.CmdPickupWeapon(weaponToPickup, weaponToPickup.GetComponent<WeaponPickup>().type, weaponToPickup.GetComponent<WeaponPickup>().id);
            }
        }        

        else if (Input.GetButtonDown("Pickup") && playerInPowerUpRange)
        {
            playerInPowerUpRange = false;
            if (powerupToPickup != null)
            {
                powerupController.CmdPowerupPickup(powerupToPickup);
            }
        }

        if (Input.GetButton("Fire1"))
        {
            if(activeWeapon != null)
            {
                Fire(false);
            }
        }

        else if (Input.GetButton("Fire2"))
        {
            if (activeWeapon != null)
            {
                if(activeWeapon.special)
                    Fire(true);
            }
        }

        if (Input.GetButton("Powerup"))
        {
            if (powerupController.powerupSlot != null)
            {
                powerupController.CmdActivation();
            }
        }

        #endregion
    }

    #region Fire

    void Fire(bool secondary)
    {
        GameObject bullet;

        if (activeWeapon.GetComponent<Weapon>().multishotAddedBullets > 0 && secondary)
        {
            bullet = activeWeapon.gameObject.GetComponent<Shuriken>().tripleshotBullet;
        }
        else
        {
            bullet = activeWeapon.bullet;
        }        

        if (secondary && activeWeapon.multishotAddedBullets <= 0)
        {
            CmdActivateWeapon();
            return;
        }

        if (activeWeapon.GetComponent<Weapon>().multishotAddedBullets > 0 && activeWeapon.maxBulletsLeft <= 0 + activeWeapon.multishotAddedBullets)
            return;

        if (activeWeapon.reloadTimeLeft > Time.time || activeWeapon.fireRateLeft > Time.time || activeWeapon.maxBulletsLeft <= 0)
            return;

        // Bullets/Reload time/Firerate
        if(secondary)
        {
            activeWeapon.bulletsLeftInMagazine -= 1 + activeWeapon.multishotAddedBullets;
            activeWeapon.maxBulletsLeft -= 1 + activeWeapon.multishotAddedBullets;
        }
        else
        {
            activeWeapon.bulletsLeftInMagazine--;
            activeWeapon.maxBulletsLeft--;
        }

        if (activeWeapon.bulletsLeftInMagazine <= ((secondary) ? 0 + activeWeapon.multishotAddedBullets : 0))
        {
            if (activeWeapon.bulletsLeftInMagazine < activeWeapon.maxBulletsLeft)
            {
                activeWeapon.bulletsLeftInMagazine = activeWeapon.bulletsInMagazine;
                activeWeapon.reloadTimeLeft = Time.time + activeWeapon.reloadTime;
            }
            else if (activeWeapon.bulletsLeftInMagazine >= activeWeapon.maxBulletsLeft)
            {
                activeWeapon.reloadTimeLeft = Time.time + activeWeapon.reloadTime;
                activeWeapon.bulletsLeftInMagazine = activeWeapon.maxBulletsLeft;
                activeWeapon.maxBulletsLeft = 0;
            }
        }

        activeWeapon.fireRateLeft = Time.time + activeWeapon.fireRate;

        // Bullet & bullet spawn point
        bulletSpawn = activeWeapon.firePoint;

        // Triple shot
        if(activeWeapon.multishotAddedBullets > 0 && secondary)
        {
            CmdSpawnMultiShot(bullet.name);
        }
        // Normal bullets
        else
            CmdSpawnBullet(bullet.name);
    }

    [Command]
    void CmdActivateWeapon()
    {
        RpcActivateWeapon();
    }

    [ClientRpc]
    void RpcActivateWeapon()
    {
        Weapon activeWeapon = wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>();

        if (activeWeapon == null)
            return;

        for (int i = 0; i < activeWeapon.bulletsAlive.Count; i++)
        {
            if (activeWeapon.bulletsAlive[i] != null)
            {
                activeWeapon.bulletsAlive[i].GetComponent<Bullet>().Activate();
            }
        }
    }

    [Command]
    void CmdSpawnBullet(string _bullet)
    {
        RpcSpawnBullet(_bullet);
    }

    [ClientRpc]
    void RpcSpawnBullet(string _bullet)
    {
        GameObject bullet = null;

        for (int i = 0; i < netMg.spawnPrefabs.Count; i++)
        {
            if(netMg.spawnPrefabs[i].name == _bullet)
            {
                bullet = netMg.spawnPrefabs[i];
            }
        }

        if (bullet == null)
            return;

        Weapon activeWeapon = wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>();

        bulletSpawn = activeWeapon.firePoint;

        GameObject clone = (GameObject)Instantiate(bullet, bulletSpawn.position, bulletSpawn.parent.rotation);
        if (doubledDamage) clone.GetComponent<Bullet>().damage *= 2;

        activeWeapon.AddBullet(clone);
        clone.GetComponent<Bullet>().weaponParent = activeWeapon.gameObject;

        if (clone.GetComponent<Bullet>().type == Bullet.BulletType.stream)
            clone.transform.parent = bulletSpawn.transform;

        // Get netId for detecting collision (so bullet doesn't collide with this player)
        var bulletStats = clone.GetComponent<Bullet>();
        bulletStats.playerId = GetComponent<NetworkBehaviour>().netId;

        // Add velocity before spawning on server
        if(clone.GetComponent<Bullet>().type != Bullet.BulletType.rail)
            clone.GetComponent<Rigidbody2D>().velocity = bulletSpawn.transform.forward * bulletStats.bulletSpeed;
    }

    [Command]
    void CmdSpawnMultiShot(string _specialBullet)
    {
        RpcSpawnMultiShot(_specialBullet);
    }

    [ClientRpc]
    void RpcSpawnMultiShot(string _specialBullet)
    {
        GameObject bullet = null;

        for (int i = 0; i < netMg.spawnPrefabs.Count; i++)
        {
            if (netMg.spawnPrefabs[i].name == _specialBullet)
            {
                bullet = netMg.spawnPrefabs[i];
            }
        }

        if (bullet == null)
            return;

        Weapon activeWeapon = wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>();

        for (int i = 0; i < activeWeapon.multishotAddedBullets + 1; i++)
        {
            bulletSpawn = activeWeapon.multiFirePoints[i];           

            // Spawn bullet at bulletSpawn
            GameObject clone = (GameObject)Instantiate(bullet, bulletSpawn.position, bulletSpawn.parent.rotation);
            activeWeapon.AddBullet(clone);
            clone.GetComponent<Bullet>().weaponParent = activeWeapon.gameObject;

            // Get netId for detecting collision (so bullet doesn't collide with this player)
            var bulletStats = clone.GetComponent<Bullet>();
            bulletStats.playerId = GetComponent<NetworkBehaviour>().netId;

            // Add velocity before spawning on server
            clone.GetComponent<Rigidbody2D>().velocity = bulletSpawn.transform.forward * bulletStats.bulletSpeed;
        }
    }

    #endregion

    #region Triggering triggers (kek)

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!isLocalPlayer)
            return;

        if (other.gameObject.layer == 10)
        {
            playerInPickupRange = true;
            weaponToPickup = other.gameObject;
        }

        else if (other.gameObject.layer == 12) //Powerup layer 
        {
            playerInPowerUpRange = true;
            powerupToPickup = other.gameObject;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!isLocalPlayer)
            return;

        if (other.gameObject.layer == 10)
        {
            playerInPickupRange = true;
            weaponToPickup = other.gameObject;
        }

        else if (other.gameObject.layer == 12) //Powerup layer 
        {
            playerInPowerUpRange = true;
            powerupToPickup = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!isLocalPlayer)
            return;

        if (other.gameObject.layer == 10)
        {
            playerInPickupRange = false;
            weaponToPickup = null;
        }

        else if (other.gameObject.layer == 12) //Powerup layer 
        {
            playerInPowerUpRange = false;
            powerupToPickup = null;
        }
    }

    #endregion
}
