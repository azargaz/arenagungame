﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Stats : NetworkBehaviour
{
    public RectTransform healthBar;
    public const float maxHealth = 100;
    [SyncVar(hook = "OnChangeHealth")]
    public float currentHealth = maxHealth;

    public const float maxArmor = 100;
    public float currentArmor = 0;

    public GameObject bloodParticles;
    Animator anim;

    //DoubleArmor powerup variable. If player activates this powerup, he's getting x2 lower damage. In this case it's simply divided and rounded to int
    [HideInInspector] public bool doubleArmor = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public override void OnStartLocalPlayer()
    {
        anim = GetComponent<Animator>();
    }

    public void TakeDamage(float amount, bool playAnim)
    {
        if (!isServer)
        {
            return;
        }

        if (doubleArmor)
        {
            amount /= 2;
            Mathf.RoundToInt(amount);
        }

        if (currentArmor > 0)
        {
            currentArmor -= amount;
            if (currentArmor < 0)
            {
                amount = Mathf.Abs(currentArmor);
                currentArmor = 0;
            }
        }
        if (currentArmor <= 0)
            currentHealth -= amount;

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Debug.Log("Dead!");
        }
        else
        {
            if(anim != null && playAnim)
            {
                anim.SetTrigger("Hit");
                RpcHitAnim();
            }

            GameObject blood = Instantiate(bloodParticles);
            blood.transform.parent = transform;
            blood.transform.localPosition = Vector2.zero;
            Destroy(blood, blood.GetComponent<ParticleSystem>().duration);
            NetworkServer.Spawn(blood);
        }
    }

    [ClientRpc]
    void RpcHitAnim()
    {
        anim.SetTrigger("Hit");
    }

    void OnChangeHealth(float health)
    {
        healthBar.GetComponent<Image>().fillAmount = health / maxHealth;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14 && gameObject.tag == "Player") //"Pickup" layer
        {
            //Making sure that player won't exceed maxArmor
            if (currentArmor + collision.gameObject.GetComponent<Pickup>().armorBoost < maxArmor)
                currentArmor += collision.gameObject.GetComponent<Pickup>().armorBoost;
            else
                currentArmor = maxArmor;

            //Making sure that player won't exceed maxHealth
            if (currentHealth + collision.gameObject.GetComponent<Pickup>().healthBoost < maxHealth)
                currentHealth += collision.gameObject.GetComponent<Pickup>().healthBoost;
            else
                currentHealth = maxHealth;

            collision.gameObject.GetComponent<Pickup>().WaitingForNextSpawn();
            collision.gameObject.layer = 0;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = null;
        }
    }
}