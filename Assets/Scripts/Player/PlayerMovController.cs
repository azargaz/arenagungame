﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(MovementController))]
public class PlayerMovController : NetworkBehaviour
{
    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    public float moveSpeed = 6;
    float oldMoveSpeed;
    public bool doubleJump;
    bool canDoubleJump;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    public float wallSlideSpeedMax = 3;
    public float wallStickTime = .25f;
    float timeToWallUnstick;

    [HideInInspector] public float gravity;
    [HideInInspector] public float jumpVelocity;
    float accelerationTimeAirborne = .1f;
    float accelerationTimeGrounded = .05f;

    [HideInInspector] public Vector3 velocity;
    float velocityXsmoothing;

    Vector2 input;

    Animator anim;
    MovementController controller;
    WeaponController wepController;
    [SerializeField]
    Camera cam;

    [SerializeField]
    GameObject arm;
    Vector2 armPos;

    [SerializeField]
    GameObject tarParticles;
    bool tar;
    float playerInTar;
    public float knockbackDuration;
    float knockbackTime;
    Vector2 knockbackPower;

    void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<MovementController>();
    }

    public override void OnStartLocalPlayer()
    {
        if (!isLocalPlayer)
            return;

        oldMoveSpeed = moveSpeed;
        anim = GetComponent<Animator>();
        controller = GetComponent<MovementController>();
        wepController = GetComponent<WeaponController>();
        armPos = arm.transform.localPosition;

        #region Movement stuff

        input = Vector2.zero;
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity * timeToJumpApex);
        //print("Gravity: " + gravity + " Jump Velocity: " + jumpVelocity);

        #endregion 
    }

    void Update()
    {
        if (!isLocalPlayer)
            return;

        #region Tar

        Tar();

        #endregion

        #region Player Input

        input = new Vector2(Input.GetAxisRaw("Horizontal"), 0);

        #endregion

        // DISABLED FOR NOW
        #region Walljumping & wallsliding & velocity smoothing

        //int wallDirX = (controller.collisions.left) ? -1 : 1;

        //float targetVelocityX = input.x * moveSpeed;
        //velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXsmoothing, controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne);


        //bool wallSliding = false;

        //if ((controller.collisions.right || controller.collisions.left) && !controller.collisions.below && velocity.y < 0)
        //{
        //    wallSliding = true;

        //    if (velocity.y < -wallSlideSpeedMax)
        //    {
        //        velocity.y = -wallSlideSpeedMax;
        //    }

        //    if (timeToWallUnstick > 0)
        //    {
        //        velocityXsmoothing = 0;
        //        velocity.x = 0;

        //        if (input.x != wallDirX && input.x != 0)
        //        {
        //            timeToWallUnstick -= Time.deltaTime;
        //        }
        //        else
        //        {
        //            timeToWallUnstick = wallStickTime;
        //        }
        //    }
        //    else
        //    {
        //        timeToWallUnstick = wallStickTime;
        //    }
        //}

        #endregion

        #region Jumping

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }

        if (controller.collisions.below && doubleJump)
        {
            canDoubleJump = true;
        }

        if (Input.GetButtonDown("Jump"))
        {
            //if (wallSliding)
            //{
            //    if (wallDirX == input.x)
            //    {
            //        velocity.x = -wallDirX * wallJumpClimb.x;
            //        velocity.y = wallJumpClimb.y;
            //    }
            //    else if (input.x == 0)
            //    {
            //        velocity.x = -wallDirX * wallJumpOff.x;
            //        velocity.y = wallJumpOff.y;
            //    }
            //    else
            //    {
            //        velocity.x = -wallDirX * wallLeap.x;
            //        velocity.y = wallLeap.y;
            //    }
            //}
            //else
            {
                if (controller.collisions.below)
                    velocity.y = jumpVelocity;
                else if (canDoubleJump)
                {
                    velocity.y = jumpVelocity;
                    canDoubleJump = false;
                }
            }
        }

        #endregion

        #region Move

        if (knockbackTime > Time.time)
        {
            input.x = knockbackPower.x;
            velocity.y = knockbackPower.y * 5;
        }

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXsmoothing, controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne);
        
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        #endregion        

        #region Sprite & arm direciton, arm rotation

        Vector3 mousePos = cam.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);

        // Flip sprite & arm, change arm position to match sprite
        if (mousePos.x - transform.position.x >= 0)
        {
            CmdChangeScale(1);
            arm.transform.localPosition = new Vector2(armPos.x, armPos.y);
        }
        else if (mousePos.x - transform.position.x < 0)
        {
            CmdChangeScale(-1);
            arm.transform.localPosition = new Vector2(-armPos.x, armPos.y);
        }

        // Rotate arm       

        if(wepController.equippedWeapons[wepController.activeSlot] == null || wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
        {
            arm.transform.GetChild(0).localEulerAngles = Vector3.zero;
            arm.transform.GetChild(0).GetChild(0).localEulerAngles = Vector3.zero;
            arm.transform.localEulerAngles = Vector3.zero;
        }
        else
        {
            arm.transform.GetChild(0).localEulerAngles = new Vector3(0, 0, -wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>().armRotation);
            arm.transform.GetChild(0).GetChild(0).localEulerAngles = new Vector3(0, 0, wepController.equippedWeapons[wepController.activeSlot].GetComponent<Weapon>().armRotation);
            arm.transform.rotation = Quaternion.LookRotation(Vector3.forward, -(mousePos - arm.transform.position));
        }

        #endregion

        #region Animations

        CmdAnimations(Mathf.Abs(input.x), controller.collisions.below);

        #endregion
    }

    #region Flip sprite & arm: change scale on client and server

    [Command]
    void CmdChangeScale(int i)
    {
        transform.FindChild("Sprite").localScale = new Vector3(i, 1, 1);
        arm.transform.localScale = new Vector2(i, 1);
        RpcChnageScale(i);
    }

    [ClientRpc]
    void RpcChnageScale(int i)
    {
        transform.FindChild("Sprite").localScale = new Vector3(i, 1, 1);
        arm.transform.localScale = new Vector2(i, 1);
    }

    #endregion

    #region Animations

    [Command]
    void CmdAnimations(float x, bool ground)
    {
        anim.SetFloat("Input", x);
        anim.SetBool("Grounded", ground);
        RpcAnimations(x, ground);
    }

    [ClientRpc]
    void RpcAnimations(float x, bool ground)
    {
        anim.SetFloat("Input", x);
        anim.SetBool("Grounded", ground);
    }

    #endregion

    void Tar()
    {
        if (tar && playerInTar < Time.time + 3)
        {
            playerInTar = Time.time + 3;
            tar = false;
        }

        if (playerInTar > Time.time)
        {
            tarParticles.SetActive(true);
            moveSpeed = 3;
        }
        else
        {
            tarParticles.SetActive(false);
            moveSpeed = oldMoveSpeed;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Tar>() != null && !tar)
        {
            tar = true;
        }
    }

    public void Knockback(Vector2 distance, float lowPower, float highPower)
    {
        if(Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {
            knockbackPower = new Vector2(highPower * Mathf.Sign(distance.x), lowPower * Mathf.Sign(distance.y));
        }
        else
        {
            knockbackPower = new Vector2(lowPower * Mathf.Sign(distance.x), highPower * Mathf.Sign(distance.y));
        }

        knockbackTime = Time.time + knockbackDuration;
    }
}