﻿using UnityEngine;
using System.Collections;

public class InvisibilityDetector : MonoBehaviour
{
    public GameObject parent;

    void OnTriggerEnter2D(Collider2D collision)
    {
        //If is touching other player
        if (collision.gameObject.layer == 8  && collision.gameObject.transform != gameObject.transform.parent.root)
        {
            SpriteRenderer[] playerColors = gameObject.transform.root.GetComponentsInChildren<SpriteRenderer>();

            for (int i = 0; i < playerColors.Length; i++)
            {
                Color temp = playerColors[i].color;
                temp.a = 1f;
                playerColors[i].color = temp;
            }

            Destroy(parent);
            Destroy(gameObject);
            Debug.Log("DESTROYED_INVISIBILITY!");
        }
    }
}