﻿using UnityEngine;
using System.Collections;

public class AmmoRestoration : PowerupController
{
    private bool isActive = false;

    public void Activate(GameObject player)
    {
        if (isActive == false)
        {
            isActive = true;
            Debug.Log("OK");

            for (int i = 0; i < 1; i++)
            {
                Weapon curWeapon = player.GetComponent<WeaponController>().equippedWeapons[i].GetComponent<Weapon>();
                curWeapon.maxBulletsLeft = curWeapon.maxBullets;
                curWeapon.bulletsLeftInMagazine = curWeapon.bulletsInMagazine;
            }            

            Destroy(gameObject);
        }
    }
}