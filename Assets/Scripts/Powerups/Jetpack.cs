﻿using UnityEngine;
using System.Collections;

public class Jetpack : PowerupController
{
    public float flyingSpeed;
    private bool isFlying = false;
    private float gravityToPass;
    private bool isActive = false;
    private GameObject _player;

    void Activate(GameObject player)
    {
        if (isActive == false)
        {
            isActive = true;
            _player = player;
            if (gravityToPass == 0)
                gravityToPass = player.GetComponent<PlayerMovController>().gravity;
        }
        //StartCoroutine(Gravity(gravityToPass, player));
    }

    void FixedUpdate()
    {
        if (isActive)
        {
            if (Input.GetButton("Powerup"))
            {
                _player.GetComponent<PlayerMovController>().gravity = 0;
                _player.transform.Translate(new Vector3(0f, flyingSpeed, 0f));
            }
            else
            {
                if (_player.GetComponent<PlayerMovController>().gravity == 0)
                    _player.GetComponent<PlayerMovController>().gravity = gravityToPass;
            }
        }
    }

    IEnumerator Gravity(float gravity, GameObject player)
    {
        yield return new WaitForSeconds(0.1f);
        player.GetComponent<PlayerMovController>().gravity = gravity;
    }
}