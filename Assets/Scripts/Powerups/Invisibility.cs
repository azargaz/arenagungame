﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Invisibility : PowerupController
{
    public float invisibilityTime;
    //public float alphaColor;
    public float triggerRadius;
    public GameObject invisibilityDetector;
    private float playerHealth;
    private float playerArmor;
    private bool isActive = false;
    private GameObject playerForFixedUpdate;

	void FixedUpdate ()
    {
        if (isActive)
        {
            

            if (playerHealth == 0)
            {
                playerHealth = playerForFixedUpdate.GetComponent<Stats>().currentHealth;
                playerArmor = playerForFixedUpdate.GetComponent<Stats>().currentArmor;
            }
            else if (playerHealth != playerForFixedUpdate.GetComponent<Stats>().currentHealth || playerArmor != playerForFixedUpdate.GetComponent<Stats>().currentArmor)
            {
                if (playerHealth < playerForFixedUpdate.GetComponent<Stats>().currentHealth || playerArmor < playerForFixedUpdate.GetComponent<Stats>().currentArmor)
                {
                    playerHealth = playerForFixedUpdate.GetComponent<Stats>().currentHealth;
                    playerArmor = playerForFixedUpdate.GetComponent<Stats>().currentArmor;
                }
                else if (playerHealth > playerForFixedUpdate.GetComponent<Stats>().currentHealth || playerArmor > playerForFixedUpdate.GetComponent<Stats>().currentArmor)
                {
                    Destroying(gameObject);
                }
            }
        }
	}

    [Command]
    void CmdInvisibility()
    {
        RpcInvisibility();
    }

    [ClientRpc]
    void RpcInvisibility()
    {
        if (playerForFixedUpdate.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            SpriteRenderer[] playerColors = playerForFixedUpdate.GetComponentsInChildren<SpriteRenderer>();

            for (int i = 0; i < playerColors.Length; i++)
            {
                Color temp = playerColors[i].color;
                temp.a = 0.5f;
                playerColors[i].color = temp;
            }
        }
        else
        {
            SpriteRenderer[] playerColors = playerForFixedUpdate.GetComponentsInChildren<SpriteRenderer>();

            for (int i = 0; i < playerColors.Length; i++)
            {
                Color temp = playerColors[i].color;
                temp.a = 0f;
                playerColors[i].color = temp;
            }
        }
    }

    void Activate(GameObject player)
    {
        if (!isActive)
        {
            playerForFixedUpdate = player;
            isActive = true;
            GameObject container = (GameObject)Instantiate(invisibilityDetector, Vector3.zero, Quaternion.identity) as GameObject;
            container.transform.parent = player.transform;
            container.transform.localPosition = Vector3.zero;
            container.GetComponent<InvisibilityDetector>().parent = gameObject;

            CircleCollider2D collider;
            if (container.GetComponent<CircleCollider2D>() == null)
            {
                collider = container.AddComponent<CircleCollider2D>();
            }
            else collider = container.GetComponent<CircleCollider2D>();

            collider.isTrigger = true;
            collider.radius = triggerRadius;
            StartCoroutine(WaitingTillInvisibiltyEnd(player));            
        }
    }

    IEnumerator WaitingTillInvisibiltyEnd(GameObject player)
    {
        yield return new WaitForSeconds(invisibilityTime);
        Destroying(player);
    }

    void Destroying(GameObject player)
    {
        SpriteRenderer[] playerColors = player.GetComponentsInChildren<SpriteRenderer>();

        for (int i = 0; i < playerColors.Length; i++)
        {
            Color temp = playerColors[i].color;
            temp.a = 1f;
            playerColors[i].color = temp;
        }

        Destroy(gameObject);
        Debug.Log("DESTROYED_INVISIBILITY!");
    }
}