﻿using UnityEngine;
using System.Collections;

public class DoubleDamage : PowerupController
{
    public float doubleDamageTime;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}

    void Activate(GameObject player)
    {
        player.GetComponent<Player>().doubledDamage = true;
        StartCoroutine(WaitingTillDoubleDamageEnd(player));
    }

    IEnumerator WaitingTillDoubleDamageEnd(GameObject player)
    {
        yield return new WaitForSeconds(doubleDamageTime);
        Destroying(player);
    }

    void Destroying(GameObject player)
    {
        player.GetComponent<Player>().doubledDamage = false;
        Destroy(gameObject);
    }
}