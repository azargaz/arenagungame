﻿using UnityEngine;
using System.Collections;

public class DoubleArmor : PowerupController
{
    public float doubleArmorTime;

    void Activate(GameObject player)
    {
        player.GetComponent<Stats>().doubleArmor = true;
        StartCoroutine(WaitingTillDoubleArmorEnd(player));
    }

    IEnumerator WaitingTillDoubleArmorEnd(GameObject player)
    {
        yield return new WaitForSeconds(doubleArmorTime);
        Destroying(player);
    }

    void Destroying(GameObject player)
    {
        player.GetComponent<Stats>().doubleArmor = false;
        Destroy(gameObject);
    }
}