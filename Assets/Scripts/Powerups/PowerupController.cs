﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PowerupController : NetworkBehaviour
{
    public GameObject player;
    public GameObject powerupSlot;

    void Update()
    {
        if (!isLocalPlayer)
            return;
    }

    [Command]
    public void CmdPowerupPickup(GameObject powerup)
    {
        powerupSlot = (GameObject)Instantiate(powerup, Vector3.zero, Quaternion.identity);
        Destroy(powerup);
    }

    [Command]
    public void CmdActivation()
    {
        RpcActivation();
    }

    [ClientRpc]
    public void RpcActivation()
    {
        if (powerupSlot != null)
            powerupSlot.SendMessage("Activate", player);
    }
}