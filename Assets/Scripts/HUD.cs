﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    WeaponController wp;
    public Sprite def;
    public Text ammo;

    public enum Slot { main, utility, powerup };
    public Slot type;

    void Awake()
    {
        wp = transform.root.GetComponent<WeaponController>();
    }

    void Update()
    {
        switch (type)
        {
            case Slot.main:
                {
                    if (wp.equippedWeapons[0] != null)
                    {
                        GetComponent<Image>().sprite = wp.equippedWeapons[0].GetComponent<SpriteRenderer>().sprite;
                        ammo.text = wp.equippedWeapons[0].GetComponent<Weapon>().maxBulletsLeft.ToString();
                    }
                    else
                    {
                        GetComponent<Image>().sprite = def;
                        ammo.text = "";
                    }

                    break;
                }
            case Slot.utility:
                {
                    if(wp.equippedWeapons[1] != null)
                    {
                        GetComponent<Image>().sprite = wp.equippedWeapons[1].GetComponent<SpriteRenderer>().sprite;
                        ammo.text = wp.equippedWeapons[1].GetComponent<Weapon>().maxBulletsLeft.ToString();
                    }
                    else
                    {
                        GetComponent<Image>().sprite = def;
                        ammo.text = "";
                    }

                    break;
                }
            case Slot.powerup:
                {
                    GetComponent<Image>().sprite = def;

                    break;
                }
        }

    }
}
