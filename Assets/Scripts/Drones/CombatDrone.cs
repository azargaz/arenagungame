﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatDrone : Weapon
{
    public float damagePerHit;
    public float hitsPerSecond;
    private bool checkingIfActive;
    private float curHealth;
    public float droneHealth;
    public bool destroyed = false;
    public float triggerRadius;
    public LayerMask playerMask;
    [HideInInspector] public bool attackingCRisRunning;
    [HideInInspector] public List<GameObject> players = new List<GameObject>();
    [HideInInspector] public GameObject playerToAttack;

    void Start()
    {
        bullet = null;
        firePoint = null;
        checkingIfActive = true;
        //gameObject.layer = ;
    }

    void FixedUpdate()
    {
        if (!destroyed && gameObject.transform.parent != null)
        {
            if (checkingIfActive)
            {
                if (gameObject.transform.parent.root.tag == "Player")
                {
                    if (!gameObject.GetComponent<CircleCollider2D>())
                    {
                        gameObject.AddComponent<CircleCollider2D>();
                        gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
                        gameObject.GetComponent<CircleCollider2D>().radius = triggerRadius;
                    }
                    curHealth = gameObject.transform.parent.root.GetComponent<Stats>().currentHealth;
                    checkingIfActive = false;
                }
            }

            if (gameObject.transform.parent.root.GetComponent<Stats>().currentHealth != curHealth)
            {
                if (gameObject.transform.parent.root.GetComponent<Stats>().currentHealth < curHealth)
                {
                    droneHealth -= (curHealth - gameObject.transform.parent.root.GetComponent<Stats>().currentHealth);
                    curHealth = gameObject.transform.parent.root.GetComponent<Stats>().currentHealth;
                }
                else if (gameObject.transform.parent.root.GetComponent<Stats>().currentHealth > curHealth)
                {
                    curHealth = gameObject.transform.parent.root.GetComponent<Stats>().currentHealth;
                }

                if (droneHealth <= 0)
                {
                    destroyed = true;
                    if (attackingCRisRunning == true)
                    {
                        StopCoroutine(Attacking());
                        attackingCRisRunning = false;
                    }
                }
            }
        }
    }

    IEnumerator Attacking()
    {
        if (!destroyed && players.Contains(playerToAttack))
        {
            if (playerToAttack == null)
            {
                ReloadingPlayers();
            }
            else
            {
                Vector2 localPlayerMax = new Vector2(transform.parent.root.GetComponent<BoxCollider2D>().bounds.max.x,
                    transform.parent.root.GetComponent<BoxCollider2D>().bounds.max.y);
                Vector2 localPlayerMin = new Vector2(transform.parent.root.GetComponent<BoxCollider2D>().bounds.min.x,
                    transform.parent.root.GetComponent<BoxCollider2D>().bounds.min.y);
                Vector2 attackedPlayerMax = new Vector2(playerToAttack.GetComponent<BoxCollider2D>().bounds.max.x,
                    playerToAttack.GetComponent<BoxCollider2D>().bounds.max.y);
                Vector2 attackedPlayerMin = new Vector2(playerToAttack.GetComponent<BoxCollider2D>().bounds.min.x,
                    playerToAttack.GetComponent<BoxCollider2D>().bounds.min.y);

                RaycastHit2D hitMax = Physics2D.Linecast(localPlayerMax, attackedPlayerMax, playerMask);
                RaycastHit2D hitMin = Physics2D.Linecast(localPlayerMin, attackedPlayerMin, playerMask);

                //RaycastHit2D hitMax = Physics2D.Raycast(localPlayerMax, new Vector2(attackedPlayerMax.x - localPlayerMax.x, attackedPlayerMax.y - localPlayerMax.y), playerMask);
                //RaycastHit2D hitMin = Physics2D.Raycast(localPlayerMax, new Vector2(attackedPlayerMin.x - localPlayerMin.x, attackedPlayerMin.y - localPlayerMin.y), playerMask);
                Debug.DrawLine(localPlayerMax, attackedPlayerMax, Color.red, 1f);
                Debug.DrawLine(localPlayerMin, attackedPlayerMin, Color.red, 1f);


                if ((hitMax.collider != null && hitMax.collider.gameObject.layer == 8) || (hitMin.collider != null && hitMin.collider.gameObject.layer == 8))
                {
                    //hitMax.collider.gameObject.GetComponent<Stats>().TakeDamage(damagePerHit, false);
                    playerToAttack.GetComponent<Stats>().TakeDamage(damagePerHit, false);
                }

                yield return new WaitForSeconds(hitsPerSecond);
                StartCoroutine(Attacking());
            }
            yield return null;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        //Debug.Log("STAY");
        if (!destroyed && gameObject.transform.parent != null)
        {
            if (collision.gameObject.layer == 8 && collision.gameObject != gameObject.transform.parent.root)
            {
                if (!players.Contains(collision.gameObject))
                {
                    players.Add(collision.gameObject);
                    playerToAttack = players[Random.Range(0, players.Count - 1)];
                    if (attackingCRisRunning == false)
                    {
                        attackingCRisRunning = true;
                        StartCoroutine(Attacking());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("EXIT");
        if (!destroyed && gameObject.transform.parent != null)
        {
            if (collision.gameObject.layer == 8 && collision.gameObject != gameObject.transform.parent.root)
            {
                if (players.Contains(collision.gameObject))
                {
                    players.Remove(collision.gameObject);
                    if (players.Count > 0)
                    {
                        Debug.Log(players.Count);
                        playerToAttack = players[Random.Range(0, players.Count - 1)];
                        attackingCRisRunning = true;
                        if (attackingCRisRunning == false)
                            StartCoroutine(Attacking());
                    }
                    else
                    {
                        if (attackingCRisRunning == true)
                        {
                            StopCoroutine(Attacking());
                            attackingCRisRunning = false;
                        }
                    }
                }
            }
        }
    }

    void ReloadingPlayers()
    {
        StopCoroutine(Attacking());
        attackingCRisRunning = false;

        for (int i = 0; i < players.Count; i++)
        {
            if (players[i] == null)
            {
                Debug.Log(players[i]);
                players.Remove(players[i]);
            }
        }
        playerToAttack = players[Random.Range(0, players.Count - 1)];
        attackingCRisRunning = true;
        StartCoroutine(Attacking());
    }
}