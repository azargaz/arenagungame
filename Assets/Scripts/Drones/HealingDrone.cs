﻿using UnityEngine;
using System.Collections;

public class HealingDrone : Weapon
{
    public float healingPerSecond;
    private bool checkingIfActive;
    private float curHealth;
    public bool destroyed = false;
    public float healthCap;

    void Start ()
    {
        bullet = null;
        firePoint = null;
        checkingIfActive = true;
	}
	
	void FixedUpdate ()
    {
        if (!destroyed && gameObject.transform.parent != null)
        {
            if (checkingIfActive)
            {
                if (gameObject.transform.parent.root.tag == "Player")
                {
                    StartCoroutine(Healing());
                    checkingIfActive = false;
                }
            }

            if (gameObject.transform.parent.root.GetComponent<Stats>().currentHealth < curHealth)
            {
                destroyed = true;
                StopCoroutine(Healing());
            }

            if (curHealth == healthCap)
            {
                destroyed = true;
                StopCoroutine(Healing());
            }
        }
	}

    IEnumerator Healing()
    {
        if (!destroyed)
        {
            gameObject.transform.parent.root.GetComponent<Stats>().currentHealth += healingPerSecond;
            curHealth = gameObject.transform.parent.root.GetComponent<Stats>().currentHealth;
            yield return new WaitForSeconds(1f);
            StartCoroutine(Healing());
            yield return null;
        }
    }
}