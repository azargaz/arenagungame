﻿using UnityEngine;
using System.Collections;

public class JumpingPad : MonoBehaviour
{
    public float jumpBoost;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.GetComponent<JumpingPad>() != null && other.tag == "Player")
        {
            other.transform.root.GetComponent<PlayerMovController>().velocity.y =
                other.transform.root.GetComponent<PlayerMovController>().jumpVelocity * jumpBoost;
            other.transform.root.GetComponent<MovementController>().Move(other.transform.root.GetComponent<PlayerMovController>().velocity * Time.deltaTime);
        }
    }
}
