﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Explosion : MonoBehaviour
{
    [HideInInspector]
    public NetworkInstanceId playerId;
    [HideInInspector]
    public float damage;

    public float timeToLive;
    public bool damageYourself;
    public float selfDmgReduction;

    public float knockbackPowerL;
    public float knockbackPowerH;

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage, false);
                }

                var controller = hit.GetComponent<PlayerMovController>();
                if (controller != null)
                {
                    controller.Knockback(hit.transform.position - transform.position, knockbackPowerL, knockbackPowerH);
                }
            }
            else if (damageYourself)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage / selfDmgReduction, false);
                }

                var controller = hit.GetComponent<PlayerMovController>();
                if (controller != null)
                {
                    controller.Knockback(hit.transform.position - transform.position, knockbackPowerL, knockbackPowerH);
                }
            }
        }
    }
}

