﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class ElectricLineBullet : Bullet
{

    [HideInInspector] public Vector3 startPos;
    [HideInInspector] public Quaternion rotation;
    [HideInInspector] public Vector3 endPos;
    public GameObject electricLineConnection;
    [HideInInspector] public bool raycastHitFinished;
    public LayerMask groundMask;
    private const float pi = 22 / 7;

	void Awake ()
    {
        startPos = gameObject.transform.position;
        rotation = gameObject.transform.rotation;
        raycastHitFinished = false;
	}
	
	void Update ()
    {

    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            // Check if netId of collided player is NOT equal to player who shot this bullet
            if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage, false);
                }

                Destroy(gameObject);
            }
        }
        else if (collision.gameObject.layer == 9)
        {
            endPos = gameObject.transform.position;
            GameObject electricLine = (GameObject)Instantiate(electricLineConnection, startPos, rotation) as GameObject;
            electricLine.transform.localScale = new Vector3(electricLine.transform.localScale.x, Mathf.Abs(startPos.y - endPos.y), electricLine.transform.localScale.z);
            rotation = new Quaternion(0, 0, 0, 0);
            //electricLine.transform.position = new Vector3(electricLine.transform.position.x, Mathf.Abs((endPos.y - startPos.y) * 0.5f) + startPos.y, electricLine.transform.position.z);
            CalculatingPoint(electricLine);
            //Vector3[] positions = new Vector3[2];
            //positions[0] = startPos;
            //positions[1] = endPos;
            //electricLine.GetComponent<LineRenderer>().SetPositions(positions);

            Destroy(gameObject);
        }
        else if (collision.gameObject.layer == 11) // Holoshield layer
        {
            collision.gameObject.GetComponent<Holoshield>().TakeDamage(gameObject.GetComponent<Bullet>().damage);
            Destroy(gameObject);
        }
    }

    void CalculatingPoint(GameObject electricLine)
    {
        Vector3 bottomCol = new Vector3(electricLine.GetComponent<BoxCollider2D>().bounds.min.x, electricLine.GetComponent<BoxCollider2D>().bounds.min.y, 0f);
        RaycastHit2D bottomHit = Physics2D.Raycast(bottomCol, Vector2.down, 0.005f, groundMask);

        if (!bottomHit)
        {
            electricLine.transform.position = new Vector3(electricLine.transform.position.x, electricLine.transform.position.y - 0.05f, electricLine.transform.position.z);
        }
        else
        {
            startPos = bottomCol;
            float angle = Mathf.Abs(Mathf.Atan2(startPos.y, endPos.x) * 180 / pi);
            Debug.Log(angle);
            electricLine.transform.localScale = new Vector3(electricLine.transform.localScale.x, Mathf.Abs(startPos.y - endPos.y), electricLine.transform.localScale.z);
            electricLine.transform.position = new Vector3(Mathf.Abs((endPos.x - startPos.x) * 0.5f) + startPos.x, Mathf.Abs((endPos.y - startPos.y) * 0.5f) + startPos.y, 1f);
            rotation = new Quaternion (0, 0, angle, 0);
        }
    }

    void CalculatingLine(GameObject electricLine)
    {
        if (raycastHitFinished)
        {
            //There is nothing, just script is finished
        }

        else
        {
            Vector2 topCol = new Vector2(electricLine.GetComponent<BoxCollider2D>().bounds.max.x, electricLine.GetComponent<BoxCollider2D>().bounds.max.y);
            Vector2 bottomCol = new Vector2(electricLine.GetComponent<BoxCollider2D>().bounds.min.x, electricLine.GetComponent<BoxCollider2D>().bounds.min.y);

            RaycastHit2D topHit = Physics2D.Raycast(topCol, Vector2.up, 0.005f, groundMask);
            RaycastHit2D bottomHit = Physics2D.Raycast(bottomCol, Vector2.down, 0.005f, groundMask);

            if (topHit)
            {
                electricLine.transform.localScale = new Vector3(electricLine.transform.localScale.x, electricLine.transform.localScale.y + 0.05f, electricLine.transform.localScale.z);
                electricLine.transform.position = new Vector3(electricLine.transform.position.x, electricLine.transform.position.y - 0.10f, electricLine.transform.position.z);
            }
            else if (bottomHit)
            {
                electricLine.transform.localScale = new Vector3(electricLine.transform.localScale.x, electricLine.transform.localScale.y + 0.05f, electricLine.transform.localScale.z);
                electricLine.transform.position = new Vector3(electricLine.transform.position.x, electricLine.transform.position.y + 0.10f, electricLine.transform.position.z);
            }
            else if (!topHit && !bottomHit)
            {
                raycastHitFinished = true;
                //   electricLine.transform.position = new Vector3(startPos.x, electricLine.transform.position.y, electricLine.transform.position.z);
            }

            CalculatingLine(electricLine);
        }
    }
}