﻿using UnityEngine;
using System.Collections;

public class GranadeLauncherBullet : Bullet
{
    public float reducingBounciness;
    protected PhysicsMaterial2D material;
    public GameObject explosion;

    void Start ()
    {
        gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.5f;
        material = new PhysicsMaterial2D("tempMaterial");
        material.bounciness = gameObject.GetComponent<CircleCollider2D>().sharedMaterial.bounciness;
        material.friction = gameObject.GetComponent<CircleCollider2D>().sharedMaterial.friction;
        gameObject.GetComponent<CircleCollider2D>().sharedMaterial = material;
    }
	
	void FixedUpdate ()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            PhysicsMaterial2D material = gameObject.GetComponent<CircleCollider2D>().sharedMaterial;
            if (material.bounciness > 0)
            {
                if (material.bounciness - reducingBounciness <= 0)
                    material.bounciness = 0;
                else
                    material.bounciness -= reducingBounciness;
            }
        }
    }

    void OnDestroy()
    {
        Destroy(material);
        GameObject clone = (GameObject)Instantiate(explosion, transform.position, Quaternion.identity);
        clone.GetComponent<Bullet>().damage = damage;
        Destroy(clone, clone.GetComponent<Bullet>().timeToLive);
    }
}