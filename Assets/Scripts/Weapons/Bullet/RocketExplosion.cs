﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class RocketExplosion : Bullet
{
    public float knockbackPowerL;
    public float knockbackPowerH;    

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage, false);
                }
                var controller = hit.GetComponent<PlayerMovController>();
                if (controller != null)
                {
                    controller.Knockback(hit.transform.position - transform.position, knockbackPowerL, knockbackPowerH);
                }
            }    
            else
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage / 2, false);
                }
                var controller = hit.GetComponent<PlayerMovController>();
                if (controller != null)
                {
                    controller.Knockback(hit.transform.position - transform.position, knockbackPowerL, knockbackPowerH);
                }
            }        
        }
    }
}
