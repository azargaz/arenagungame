﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HoloshieldBullet : Bullet
{
    public GameObject holoshield;

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;

            GameObject shield = (GameObject)Instantiate(holoshield, gameObject.transform.position, Quaternion.identity) as GameObject;
            Destroy(gameObject);
        }

        else if (collision.gameObject.layer == 11)
        {
            Destroy(gameObject);
        }
    }
}