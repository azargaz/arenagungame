﻿using UnityEngine;
using System.Collections;

public class Tar : MonoBehaviour
{
    public float timeToLive;

	void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Destroy(gameObject, timeToLive);
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }
}
