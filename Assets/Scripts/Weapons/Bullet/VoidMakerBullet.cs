﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class VoidMakerBullet : Bullet
{
    public override void Activate()
    {
        VoidMakerBullet[] bullets = FindObjectsOfType<VoidMakerBullet>();
        for (int i = 0; i < bullets.Length; i++)
        {
            Instantiate(toSpawn, bullets[i].transform.position, Quaternion.identity);
            Destroy(bullets[i].gameObject);
        }
    }
}