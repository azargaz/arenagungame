﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public abstract class Bullet : NetworkBehaviour
{
    public enum BulletType { bullet, rail, stream };

    [Header("Standard attributes")]
    public BulletType type;    
    public float bulletSpeed;
    public float timeToLive;
    public float damage;
    public LayerMask collisionMask;
    public GameObject destroyParticles;

    [Header("Gravitation and rotation while flying")]
    public bool rotate;
    public float gravityScale;

    [Header("Spawning objetcs on collision")]
    public bool spawnOnCollide;
    public LayerMask spawnMask;
    public GameObject toSpawn;

    [Header("Freeze on collision with wall")]
    public bool freezeOnCollide;

    [Header("Player knockback")]
    public bool knockback;
    public float lowKnockbackPower;
    public float highKnockbackPower;

    [Header("BulletType: stream")]
    public float dmgCooldown;
    float dmgCD;    

    [Header("Other (playerId and bullet specific attributes)")]
    public NetworkInstanceId playerId;

    [HideInInspector]
    public GameObject weaponParent;
    RaycastHit2D hit;

    protected virtual void Awake()
    {            
        GetComponent<Rigidbody2D>().gravityScale = gravityScale;        
    }

    void Start()
    {
        Destroy(gameObject, timeToLive);
    }

    protected virtual void Update()
    {      
        if (hit)
            return;

        if (type != BulletType.rail)
            hit = Physics2D.Raycast(transform.position, GetComponent<Rigidbody2D>().velocity, GetComponent<Rigidbody2D>().velocity.magnitude * Time.deltaTime, collisionMask);
        else
        {
            hit = Physics2D.Raycast(transform.position, -weaponParent.GetComponent<Weapon>().firePoint.transform.up, 20, collisionMask);

            LineRenderer lr = GetComponent<LineRenderer>();

            lr.SetPosition(0, weaponParent.GetComponent<Weapon>().firePoint.position);
            if (hit)
                lr.SetPosition(1, hit.point);
            else
                lr.SetPosition(1, weaponParent.GetComponent<Weapon>().range.position);
        }

        if (hit)
        {
            if(spawnOnCollide)
            {
                if (type != BulletType.stream)
                    transform.position = Vector2.Lerp(transform.position, hit.point, 0.1f);
                OnTriggerEnter2D(hit.collider);
            }
            else
            {
                if(hit.transform.gameObject.layer == 8)
                {
                    if(playerId != hit.transform.gameObject.GetComponent<NetworkBehaviour>().netId)
                    {
                        if (type != BulletType.stream)
                            transform.position = Vector2.Lerp(transform.position, hit.point, 0.1f);
                        OnTriggerEnter2D(hit.collider);
                    }                    
                }
                else
                {
                    if (type != BulletType.stream)
                        transform.position = Vector2.Lerp(transform.position, hit.point, 0.1f);
                    OnTriggerEnter2D(hit.collider);
                }                
            }
        }

        #region Rotate

        if (rotate)
        {
            float rotZ = (Mathf.Atan2(gameObject.GetComponent<Rigidbody2D>().velocity.y, gameObject.GetComponent<Rigidbody2D>().velocity.x) * Mathf.Rad2Deg);
            transform.rotation = Quaternion.Euler(0, 0, rotZ + 90);
        }

        #endregion

        #region Bullet type: stream

        if (isLocalPlayer && !Input.GetButton("Fire1") && type == BulletType.stream)
        {
            CmdDestroyStream();
        }

        #endregion
    }

    [Command]
    void CmdDestroyStream()
    {
        RpcDestroyStream();
    }

    [ClientRpc]
    void RpcDestroyStream()
    {
        Destroy(gameObject);
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        #region Knockback

        if(knockback && collision.gameObject.layer == 8 && playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId)
        {
            collision.gameObject.GetComponent<PlayerMovController>().Knockback(collision.transform.position - transform.position, lowKnockbackPower, highKnockbackPower);
        }

        #endregion

        #region Bullet type: stream

        if (type == BulletType.stream && collision.gameObject.layer == 8)
        {
            if(collision.gameObject.layer == 8)
            {
                if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId && dmgCD <= Time.time)
                {
                    var hit = collision.gameObject;
                    var health = hit.GetComponent<Stats>();
                    if (health != null)
                    {
                        health.TakeDamage(damage, false);
                    }

                    dmgCD = Time.time + dmgCooldown;
                }
            }            

            return;
        }

        #endregion

        #region Holoshield collision

        if (collision.gameObject.layer == 11)
        {
            collision.gameObject.GetComponent<Holoshield>().TakeDamage(gameObject.GetComponent<Bullet>().damage);
            DestroyBullet();
        }

        #endregion  

        #region Freeze on collide (with wall)

        if (freezeOnCollide && collision.gameObject.layer == 9)
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            Destroy(this);
            return;
        }

        #endregion

        #region Spawn on collide

        if(spawnOnCollide)
        {
            SpawnOnCollide(collision);
            return;
        }

        #endregion

        #region Player collision

        if (collision.gameObject.layer == 8)
        {
            // Check if netId of collided player is NOT equal to player who shot this bullet
            if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage, false);
                }

                DestroyBullet();
            }           
        }

        #endregion

        #region Ground collision

        else if (collision.gameObject.layer == 9)
        {
            DestroyBullet();
        }

        #endregion              
    }

    public virtual void OnTriggerStay2D(Collider2D collision)
    {
        #region Bullet type: stream

        if (type == BulletType.stream && collision.gameObject.layer == 8)
        {
            if (playerId != collision.gameObject.GetComponent<NetworkBehaviour>().netId && dmgCD <= Time.time)
            {
                var hit = collision.gameObject;
                var health = hit.GetComponent<Stats>();
                if (health != null)
                {
                    health.TakeDamage(damage, false);
                }

                dmgCD = Time.time + dmgCooldown;
            }

            return;
        }

        #endregion
    }

    #region DestroyBullet

    void DestroyBullet(float optionalDelay = 0)
    {
        if (type == BulletType.rail)
            return;

        weaponParent.GetComponent<Weapon>().bulletsAlive.Remove(gameObject);

        Invoke("_DestroyBullet", optionalDelay);
    }

    // Needed to delay particles spawn
    void _DestroyBullet()
    {
        if (destroyParticles != null)
        {
            GameObject _spawn = (GameObject)Instantiate(destroyParticles, transform.position, Quaternion.identity);

            Destroy(_spawn, _spawn.GetComponent<ParticleSystem>().duration);
        }

        Destroy(gameObject);
    }

    #endregion

    #region Spawn on collide

    void SpawnOnCollide(Collider2D collision)
    {
        if (spawnMask == (spawnMask | (1 << collision.gameObject.layer)))
        {
            GameObject _spawn = (GameObject)Instantiate(toSpawn, transform.position, Quaternion.identity);

            if (_spawn.GetComponent<Explosion>() != null)
            {
                Explosion boom = _spawn.GetComponent<Explosion>();
                boom.damage = damage;   
                boom.playerId = playerId;
                Destroy(_spawn, boom.timeToLive);
            }

            DestroyBullet();
        }
    }

    #endregion

    public virtual void Activate()
    {

    }
}
