﻿using UnityEngine;
using System.Collections;

public class GranadeExplosion : Bullet
{
    public float knockbackPowerL;
    public float knockbackPowerH;

    void OnParticleCollision(GameObject collision)
    {
        if (collision.layer == 8)
        {
            var hit = collision;
            var health = hit.GetComponent<Stats>();
            if (health != null)
            {
                health.TakeDamage(damage, false);
            }

            var controller = hit.GetComponent<PlayerMovController>();
            if (controller != null)
            {
                controller.Knockback(hit.transform.position - transform.position, knockbackPowerL, knockbackPowerH);
            }
        }
    }
}