﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class WeaponController : NetworkBehaviour
{
    [Header("Weapon list")]
    public List<GameObject> weapons;
    [Header("Weapon pickups list")]
    public List<GameObject> weaponPickups;
    [Header("Equipped weapons (max 2)")]
    public GameObject[] equippedWeapons;
    [Header("Active weapon slot")]
    public int activeSlot = 0;

    void Awake()
    {
        activeSlot = 0;
    }

    void Update()
    {
        if (!isLocalPlayer)
            return;

        if (Input.GetButtonDown("SwapWeapon"))
        {
            CmdSwitchWeapon();
        }
    }

    [Command]
    void CmdSwitchWeapon()
    {
        RpcSwitchWeapon();
    }

    [ClientRpc]
    void RpcSwitchWeapon()
    {
        activeSlot = (activeSlot == 0) ? 1 : 0;

        if (activeSlot == 0)
        {
            if(equippedWeapons[0] != null)
                equippedWeapons[0].SetActive(true);

            if(equippedWeapons[1] != null)
                equippedWeapons[1].SetActive(false);
        }
        else
        {
            if (equippedWeapons[1] != null)
                equippedWeapons[1].SetActive(true);

            if (equippedWeapons[0] != null)
                equippedWeapons[0].SetActive(false);
        }
    }

    [Command]
    public void CmdPickupWeapon(GameObject weapon, int type, int id)
    {
        NetworkServer.Destroy(weapon);

        if (equippedWeapons[type] != null)
        {
            for (int i = 0; i < weaponPickups.Count; i++)
            {
                if (weaponPickups[i].name.Contains(equippedWeapons[type].GetComponent<Weapon>().weaponName))
                {
                    NetworkServer.Spawn((GameObject) Instantiate(weaponPickups[i], transform.position, Quaternion.identity));
                }
            }
        }

        RpcPickupWeapon(type, id);
    }

    [ClientRpc]
    void RpcPickupWeapon(int type, int id)
    {
        if (activeSlot != type)       
            CmdSwitchWeapon();
        
        if (equippedWeapons[activeSlot] != null)
            equippedWeapons[activeSlot].SetActive(false);

        equippedWeapons[type] = weapons[id];
        equippedWeapons[type].SetActive(true);
    }

    // OLD WEAPON CONTROLLER

    //public Player player;
    //public GameObject hand;
    //public GameObject droneSlot;
    //public int activeSlot;

    //public List<int> maxBullets;
    //public List<int> bulletsInMagazine;

    //public List<GameObject> weapons;
    //public List<GameObject> equippedWeapon;

    //#region Server Spawn and Destroy

    //[Command]
    //void CmdSpawn(GameObject weapon)
    //{
    //    NetworkServer.Spawn(weapon);
    //}

    //[Command]
    //void CmdDestroy(GameObject weapon)
    //{
    //    Destroy(weapon);
    //}

    //#endregion

    //void Update()
    //{
    //    if (!isLocalPlayer)
    //        return;

    //    if (Input.GetButtonDown("SwapWeapon"))
    //    {
    //        CmdSwapWeapon();
    //    }

    //    if(Input.GetButtonDown("Drop"))
    //    {
    //        CmdDropWeapon();
    //    }        
    //}

    //#region Swap weapon

    //// Swap on server
    //[Command]
    //public void CmdSwapWeapon()
    //{
    //    // Swap active weapon slot
    //    activeSlot = (activeSlot == 1) ? 0 : 1;

    //    // if it's not empty set sprite of weapon, else set sprite to null
    //    if (equippedWeapon[activeSlot] != null)
    //    {
    //        if (equippedWeapon[activeSlot].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //        {
    //        }
    //        else
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[activeSlot].GetComponent<SpriteRenderer>().sprite;
    //        }
    //    }
    //    else
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }    

    //    RpcSwapWeapon(activeSlot);
    //}

    //// Swap on client
    //[ClientRpc]
    //void RpcSwapWeapon(int slot)
    //{
    //    activeSlot = slot;

    //    if (equippedWeapon[activeSlot] != null)
    //    {
    //        if (equippedWeapon[activeSlot].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = null;
    //        }
    //        else
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[activeSlot].GetComponent<SpriteRenderer>().sprite;
    //        }
    //    }
    //    else
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }
    //}

    //#endregion

    //#region Drop weapon

    //// Drop weapon on server
    //[Command]
    //public void CmdDropWeapon()
    //{
    //    // If no equipped weapon return
    //    if (equippedWeapon[activeSlot] == null)
    //        return;

    //    // Search which weapon to Instantiate and spawn it on server with CmdSpawn (so all players can see this weapon)
    //    for (int i = 0; i < weapons.Count; i++)
    //    {
    //        if (equippedWeapon[activeSlot].GetComponent<Weapon>().weaponName == weapons[i].GetComponent<Weapon>().weaponName)
    //        {
    //            if(equippedWeapon[activeSlot].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //                Destroy(droneSlot.transform.GetChild(0).gameObject);

    //            GameObject clone = (GameObject)Instantiate(weapons[i], transform.position, Quaternion.identity);

    //            clone.GetComponent<Weapon>().maxBullets = maxBullets[activeSlot];
    //            clone.GetComponent<Weapon>().bulletsInMagazine = bulletsInMagazine[activeSlot];

    //            CmdSpawn(clone);
    //        }
    //    }

    //    // Set equippedWeapon to null and sprite to null
    //    equippedWeapon[activeSlot] = null;
    //    if(activeSlot == 0)
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }
    //    else
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }

    //    RpcDropWeapon();
    //}

    //// Update sprite and equipped weapons on client
    //[ClientRpc]
    //void RpcDropWeapon()
    //{
    //    equippedWeapon[activeSlot] = null;
    //    if (activeSlot == 0)
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }
    //    else
    //    {
    //        hand.GetComponent<SpriteRenderer>().sprite = null;
    //    }
    //}

    //#endregion

    //#region Pickup weapon

    //// Pickup weapon that player is colliding with
    //[Command]
    //public void CmdPickUpWeapon(GameObject weapon)
    //{
    //    switch(weapon.GetComponent<Weapon>().type)
    //    {
    //        case Weapon.WeaponType.main:
    //            {
    //                // If there is currently equipped weapon, drop it and spawn it on server with CmdSpawn
    //                if (equippedWeapon[0] != null)
    //                {
    //                    for (int i = 0; i < weapons.Count; i++)
    //                    {
    //                        if (equippedWeapon[0].GetComponent<Weapon>().weaponName == weapons[i].GetComponent<Weapon>().weaponName)
    //                        {
    //                            GameObject clone = (GameObject)Instantiate(weapons[i], weapon.transform.position, Quaternion.identity);

    //                            clone.GetComponent<Weapon>().maxBullets = maxBullets[0];
    //                            clone.GetComponent<Weapon>().bulletsInMagazine = bulletsInMagazine[0];

    //                            CmdSpawn(clone);
    //                        }
    //                    }
    //                }

    //                // Change equipped weapons
    //                for (int i = 0; i < weapons.Count; i++)
    //                {
    //                    if (weapons[i].GetComponent<Weapon>().weaponName == weapon.GetComponent<Weapon>().weaponName)
    //                    {
    //                        equippedWeapon[0] = weapons[i];

    //                        maxBullets[0] = weapon.GetComponent<Weapon>().maxBullets;
    //                        bulletsInMagazine[0] = weapon.GetComponent<Weapon>().bulletsInMagazine;

    //                        RpcPickUpWeapon(0, i, maxBullets[0], bulletsInMagazine[0]);
    //                    }
    //                }

    //                if(activeSlot != 0)
    //                {
    //                    CmdSwapWeapon();
    //                }

    //                // Change sprite
    //                if(activeSlot == 0)
    //                {
    //                    hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[0].GetComponent<SpriteRenderer>().sprite;
    //                }

    //                break;
    //            }
    //        case Weapon.WeaponType.utility:
    //            {
    //                // If there is currently equipped weapon, drop it and spawn it on server with CmdSpawn
    //                if (equippedWeapon[1] != null)
    //                {
    //                    for (int i = 0; i < weapons.Count; i++)
    //                    {
    //                        if (equippedWeapon[1].GetComponent<Weapon>().weaponName == weapons[i].GetComponent<Weapon>().weaponName)
    //                        {
    //                            if (equippedWeapon[1].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //                                Destroy(droneSlot.transform.GetChild(0).gameObject);

    //                            GameObject clone = (GameObject)Instantiate(weapons[i], weapon.transform.position, Quaternion.identity);

    //                            clone.GetComponent<Weapon>().maxBullets = maxBullets[1];
    //                            clone.GetComponent<Weapon>().bulletsInMagazine = bulletsInMagazine[1];

    //                            CmdSpawn(clone);
    //                        }
    //                    }
    //                }

    //                // Change equipped weapons
    //                for (int i = 0; i < weapons.Count; i++)
    //                {
    //                    if (weapons[i].GetComponent<Weapon>().weaponName == weapon.GetComponent<Weapon>().weaponName)
    //                    {
    //                        equippedWeapon[1] = weapons[i];

    //                        maxBullets[1] = weapon.GetComponent<Weapon>().maxBullets;
    //                        bulletsInMagazine[1] = weapon.GetComponent<Weapon>().bulletsInMagazine;

    //                        RpcPickUpWeapon(1, i, maxBullets[1], bulletsInMagazine[1]);
    //                    }
    //                }

    //                if (activeSlot != 1)
    //                {
    //                    CmdSwapWeapon();
    //                }

    //                // Change sprite
    //                if (activeSlot == 1)
    //                {
    //                    droneSlot.GetComponent<SpriteRenderer>().sprite = null;
    //                    hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[1].GetComponent<SpriteRenderer>().sprite;
    //                }

    //                break;
    //            }
    //        case Weapon.WeaponType.drone:
    //            {
    //                // If there is currently equipped weapon, drop it and spawn it on server with CmdSpawn
    //                if (equippedWeapon[1] != null)
    //                {
    //                    for (int i = 0; i < weapons.Count; i++)
    //                    {
    //                        if (equippedWeapon[1].GetComponent<Weapon>().weaponName == weapons[i].GetComponent<Weapon>().weaponName)
    //                        {
    //                            if(equippedWeapon[1].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //                                Destroy(droneSlot.transform.GetChild(0).gameObject);

    //                            GameObject clone = (GameObject)Instantiate(weapons[i], weapon.transform.position, Quaternion.identity);

    //                            CmdSpawn(clone);
    //                        }
    //                    }
    //                }

    //                // Change equipped weapons
    //                for (int i = 0; i < weapons.Count; i++)
    //                {
    //                    if (weapons[i].GetComponent<Weapon>().weaponName == weapon.GetComponent<Weapon>().weaponName)
    //                    {
    //                        equippedWeapon[1] = weapons[i];

    //                        maxBullets[1] = weapon.GetComponent<Weapon>().maxBullets;
    //                        bulletsInMagazine[1] = weapon.GetComponent<Weapon>().bulletsInMagazine;

    //                        RpcPickUpWeapon(1, i, maxBullets[1], bulletsInMagazine[1]);
    //                    }
    //                }

    //                GameObject drone = Instantiate(equippedWeapon[1]);
    //                drone.gameObject.layer = 12;
    //                drone.transform.parent = droneSlot.transform;
    //                drone.transform.localPosition = Vector2.zero;
    //                Destroy(drone.GetComponent<BoxCollider2D>());                    
    //                CmdSpawn(drone);

    //                // Change sprite
    //                if (activeSlot == 1)
    //                {
    //                    hand.GetComponent<SpriteRenderer>().sprite = null;
    //                    CmdSwapWeapon();
    //                }

    //                break;
    //            }
    //    }
        
    //    CmdDestroy(weapon);   
    //}

    //// Change sprite and equipped weapon on client
    //[ClientRpc]
    //void RpcPickUpWeapon(int slot, int i, int max, int mag)
    //{
    //    equippedWeapon[slot] = weapons[i];

    //    maxBullets[slot] = max;
    //    bulletsInMagazine[slot] = mag;

    //    if (equippedWeapon[slot].GetComponent<Weapon>().type == Weapon.WeaponType.drone)
    //    {
    //        if(activeSlot == 1)
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = null;
    //        }
    //    }
    //    else
    //    {
    //        if (activeSlot == 1)
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[slot].GetComponent<SpriteRenderer>().sprite;
    //        }  
    //        else
    //        {
    //            hand.GetComponent<SpriteRenderer>().sprite = equippedWeapon[slot].GetComponent<SpriteRenderer>().sprite;
    //        }              
    //    }

    //}

    //#endregion      
}
