﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Weapon : MonoBehaviour
{    
    public enum WeaponType { main, utility, drone};

    [Header("Standard attributes")]
    public WeaponType type;
    public string weaponName;    
    public Transform firePoint;
    public GameObject bullet;    

    [Header ("RMB Attacks")]
    public bool special;
    public int multishotAddedBullets;
    public GameObject tripleshotBullet;
    public Transform[] multiFirePoints;

    [Header("Fire rate, reload time, ammo")]
    public float fireRate;
    [HideInInspector]
    public float fireRateLeft;
    public float reloadTime;
    [HideInInspector]
    public float reloadTimeLeft;
    [HideInInspector]
    public int bulletsInMagazine;
    public int bulletsLeftInMagazine;
    [HideInInspector]
    public int maxBullets;   
    public int maxBulletsLeft;

    [Header("Rail bullets")]
    public LayerMask collisionMask;
    public Transform aim;
    public Transform range;

    [Header("Other (arm rotation & weapon specific attributes)")]
    public float armRotation;
    [HideInInspector]
    public List<GameObject> bulletsAlive;    

    public virtual void Awake()
    {
        bulletsLeftInMagazine = bulletsInMagazine;
        maxBulletsLeft = maxBullets;
    }

    public void AddBullet(GameObject _bullet)
    {
        bulletsAlive.Add(_bullet);
    }

    public virtual void Update()
    {
        for (int i = 0; i < bulletsAlive.Count; i++)
        {
            if(bulletsAlive[i] == null)
            {
                bulletsAlive.RemoveAt(i);
            }
        }
    }
}
