﻿using UnityEngine;
using System.Collections;

public class MachineGun : Weapon
{
    public override void Update()
    {
        base.Update();

        firePoint.localRotation = Quaternion.Euler(90 + Random.Range(-10, 11), 90, 0);
    }
}
