﻿using UnityEngine;
using System.Collections;

public class SniperRifle : Weapon
{
    public override void Update()
    {
        base.Update();

        RaycastHit2D hit = Physics2D.Raycast(aim.position, -transform.up, 20, collisionMask);

        LineRenderer lr = GetComponent<LineRenderer>();

        lr.SetPosition(0, aim.position);
        if (hit)
            lr.SetPosition(1, hit.point);
        else
            lr.SetPosition(1, range.position);
    }
}
