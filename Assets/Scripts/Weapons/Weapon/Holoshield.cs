﻿using UnityEngine;
using System.Collections;

public class Holoshield : MonoBehaviour
{
    Vector2 topCol;
    Vector2 bottomCol;
    Vector2 leftCol;
    Vector2 rightCol;

    public LayerMask groundMask;
    public float health;
    [SerializeField]bool isFinished = false;
    BoxCollider2D boxCol2d;
	void Start ()
    {
        
	    if (gameObject.transform.position.y != (int)(gameObject.transform.position.y))
        {
            //gameObject.transform.position = new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z);
        }
        boxCol2d = gameObject.GetComponent<BoxCollider2D>();

        Invoke("CalculatingPosition", 0f);
    }
	
	void CalculatingPosition ()
    {
        if (!isFinished)
        {
            topCol = new Vector2(boxCol2d.bounds.max.x - (boxCol2d.size.x * gameObject.transform.localScale.x) / 2, boxCol2d.bounds.max.y);
            bottomCol = new Vector2(boxCol2d.bounds.max.x - (boxCol2d.size.x * gameObject.transform.localScale.x) / 2, boxCol2d.bounds.min.y);
            rightCol = new Vector2(boxCol2d.bounds.max.x, boxCol2d.bounds.max.y - (boxCol2d.size.y * gameObject.transform.localScale.y) / 2);
            leftCol = new Vector2(boxCol2d.bounds.min.x, boxCol2d.bounds.max.y - (boxCol2d.size.y * gameObject.transform.localScale.y) / 2);
            RaycastHit2D topHit = Physics2D.Raycast(topCol, Vector2.up, 0.01f, groundMask);
            RaycastHit2D bottomHit = Physics2D.Raycast(bottomCol, Vector2.down, 0.01f, groundMask);
            RaycastHit2D rightHit = Physics2D.Raycast(rightCol, Vector2.right, 0.01f, groundMask);
            RaycastHit2D leftHit = Physics2D.Raycast(leftCol, Vector2.left, 0.01f, groundMask);

            if (topHit)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.01f);
            }
            else if (bottomHit)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.01f);
            }
            else if (topHit && bottomHit)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.01f);
            }
            else if (leftHit)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x + 0.01f, gameObject.transform.position.y);
            }
            else if (rightHit)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x - 0.01f, gameObject.transform.position.y);
            }
            else if (topHit && (rightHit || leftHit))
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.01f);
            }


            if (topHit || bottomHit || leftHit || rightHit)
            {
                Invoke("CalculatingPosition", 0f);
            }
            else
            {
                isFinished = true;
            }
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount;

        if (health <= 0)
        {
            Destroy(gameObject);
        }   
    }


}