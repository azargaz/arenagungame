﻿using UnityEngine;
using System.Collections;

public class LightingGun : Weapon
{
    /*
    [HideInInspector] public Vector2 startPos;
    [HideInInspector] public Vector2 velocity;
    public float lightingDistance;
    public LayerMask playerMask;
    [HideInInspector] public bool gotVelocity = false;

	void Awake ()
    {
        startPos = gameObject.transform.GetChild(0).transform.position;
        //This object cannot interact with anything. It's only used for further scripting
        //gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
	}
	
	void Update ()
    {
        if (Input.GetButton("Fire1"))
        {
            GameObject clone = (GameObject)Instantiate(bullet, startPos, gameObject.transform.rotation);
            velocity = clone.GetComponent<Rigidbody2D>().velocity;
            Debug.Log(velocity);
            if (!gotVelocity && clone.GetComponent<Rigidbody2D>().velocity != Vector2.zero)
            {
                velocity = clone.GetComponent<Rigidbody2D>().velocity;
                clone.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                clone.GetComponent<Rigidbody2D>().freezeRotation = true;
                gotVelocity = true;
                ActivateLighting();
            }
        }
        else if (!Input.GetButton("Fire1") && gotVelocity)
            Destroy(gameObject);
    }

    void ActivateLighting()
    {
        if (Input.GetButton("Fire1"))
        {
            //Vector2 collider = new Vector2(gameObject.GetComponent<BoxCollider2D>().bounds.max.x, gameObject.GetComponent<BoxCollider2D>().bounds.max.y);
            //Debug.Log(collider);
            //Debug.Log(velocity);
            RaycastHit2D hit = Physics2D.Raycast(startPos, velocity, lightingDistance, playerMask);
            Debug.DrawRay(startPos, velocity, Color.red, hit.distance);
            if (hit)
            {
                Debug.Log("HIT!");
            }
        }
        else
        {
            Destroy(gameObject);
        }
        Invoke("ActivateLighting", 0f);
    }*/
}