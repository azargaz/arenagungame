﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class WeaponSpawner : NetworkBehaviour
{
    [SerializeField]
    List<GameObject> weapons;
    [SerializeField]
    List<GameObject> spawnPoints;

    public override void OnStartServer()
    {
        for (int i = 0; i < weapons.Count; i++)
        {
            if(spawnPoints[i] != null)
            {
                GameObject weapon = (GameObject)Instantiate(weapons[i], spawnPoints[i].transform.position, Quaternion.identity);
                NetworkServer.Spawn(weapon);
            }
        }
    }
}
