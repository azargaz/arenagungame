﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VoidController : MonoBehaviour
{
    public float damagePerSecond;
    public bool stayingInVoid;
    public List<GameObject> players = new List<GameObject>();
    public float invokeTime;
    public float timeToDestroy;

	void Start ()
    {
        StartCoroutine(DealingDamage());
        StartCoroutine(Destroying());
    }

	IEnumerator DealingDamage()
    {
	    if (stayingInVoid && players.Count > 0)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (invokeTime < 1)
                    players[i].GetComponent<Stats>().TakeDamage(damagePerSecond * invokeTime, false);
                else if (invokeTime > 1)
                    players[i].GetComponent<Stats>().TakeDamage(damagePerSecond / invokeTime, false);
            }
        }
        yield return new WaitForSeconds(invokeTime);
        StartCoroutine(DealingDamage());
        yield return null;
	}

    IEnumerator Destroying()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            stayingInVoid = true;
            if (!players.Contains(collider.gameObject))
                players.Add(collider.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            stayingInVoid = false;
            if (players.Contains(collider.gameObject))
                players.Remove(collider.gameObject);
        }
    }
}