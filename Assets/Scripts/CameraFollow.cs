﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public float smoothTime;

    public float maxX;
    public float maxY;
    Vector3 velocity;

    void Update()
    {
        Vector3 mousePos = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Mathf.Abs(mousePos.x - transform.parent.position.x) > maxX)
            {
                if (Mathf.Abs(mousePos.y - transform.parent.position.y) > maxY)
                {
                    Smooth(transform.position.x, transform.position.y);
                }
                else
                {
                    Smooth(transform.position.x, (transform.parent.position.y + mousePos.y) / 2);
                }
            }
            else
            {
                if (Mathf.Abs(mousePos.y - transform.parent.position.y) > maxY)
                {
                    Smooth((transform.parent.position.x + mousePos.x) / 2, transform.position.y);
                }
                else
                {
                    Smooth((transform.parent.position.x + mousePos.x) / 2, (transform.parent.position.y + mousePos.y) / 2);
                }
            }
        }
        else
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, transform.localPosition, ref velocity, smoothTime);
        }
    }

    void Smooth(float x, float y)
    {
        transform.position = Vector3.SmoothDamp(transform.position, new Vector3(x, y, transform.position.z), ref velocity, smoothTime);
    }
}
