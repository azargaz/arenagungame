﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour
{
    public float healthBoost;
    public float armorBoost;

	void Start ()
    {
	
	}

	void Update ()
    {
	
	}

    public void WaitingForNextSpawn()
    {
        if (gameObject.transform.parent.name.Contains("Pickup_health100"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().health100Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_health50"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().health50Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_health25"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().health25Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_armor100"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().armor100Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_armor50"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().armor50Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_armor25"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().armor25Spawn, gameObject.transform.parent.gameObject));
        }
        else if (gameObject.transform.parent.name.Contains("Pickup_armor5"))
        {
            StartCoroutine(gameObject.transform.parent.root.GetComponent<PickupSpawner>().RespawningPickups(gameObject.transform.parent.root.GetComponent<PickupSpawner>().armor5Spawn, gameObject.transform.parent.gameObject));
        }
    }
}