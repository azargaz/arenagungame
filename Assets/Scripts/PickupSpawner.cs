﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PickupSpawner : NetworkBehaviour
{
    public List<GameObject> spawns = new List<GameObject>();
    [SerializeField]
    GameObject pickupPrefab;
    public float health100Spawn;
    public float health50Spawn;
    public float health25Spawn;
    public float armor100Spawn;
    public float armor50Spawn;
    public float armor25Spawn;
    public float armor5Spawn;

    public override void OnStartServer()
    {
        for (int i = 0; i < spawns.Count; i++)
        {
            GameObject clone = (GameObject)Instantiate(pickupPrefab, spawns[i].transform.position, spawns[i].transform.rotation) as GameObject;
            AddingValues(spawns[i], clone);
            clone.transform.parent = spawns[i].transform;
        }
    }

    public IEnumerator RespawningPickups(float timeToWait, GameObject spawnObject)
    {
        yield return new WaitForSeconds(timeToWait);
        Respawn(spawnObject);

        yield return null;
    }

    void Respawn(GameObject spawnObject)
    {
        Destroy(spawnObject.transform.GetChild(0).gameObject);
        GameObject clone = (GameObject)Instantiate(pickupPrefab, spawnObject.transform.position, spawnObject.transform.rotation) as GameObject;
        AddingValues(spawnObject, clone);
        clone.transform.parent = spawnObject.transform;
    }

    void AddingValues(GameObject spawnObject, GameObject clone)
    {
        if (spawnObject.name.Contains("Pickup_health100"))
        {
            clone.GetComponent<Pickup>().healthBoost = 100;
        }
        else if (spawnObject.name.Contains("Pickup_health50"))
        {
            clone.GetComponent<Pickup>().healthBoost = 50;
        }
        else if (spawnObject.name.Contains("Pickup_health25"))
        {
            clone.GetComponent<Pickup>().healthBoost = 25;
        }
        else if (spawnObject.name.Contains("Pickup_armor100"))
        {
            clone.GetComponent<Pickup>().armorBoost = 100;
        }
        else if (spawnObject.name.Contains("Pickup_armor50"))
        {
            clone.GetComponent<Pickup>().armorBoost = 50;
        }
        else if (spawnObject.name.Contains("Pickup_armor25"))
        {
            clone.GetComponent<Pickup>().armorBoost = 25;
        }
        else if (spawnObject.name.Contains("Pickup_armor5"))
        {
            clone.GetComponent<Pickup>().armorBoost = 5;
        }
    }
}